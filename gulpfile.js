var gulp = require('gulp'), // Подключаем Gulp
	sass = require('gulp-less'); //Подключаем Sass пакет

gulp.task('less', function(){ // Создаем таск "sass"
	return gulp.src('css/main.less') // Берем источник
		.pipe(sass()) // Преобразуем Sass в CSS посредством gulp-sass
		.pipe(gulp.dest('build')) // Выгружаем результата в папку app/css
});